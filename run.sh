#!/bin/bash
mkdir -p cache
docker build -t christimperley/phd:repair-model .
docker run  --rm \
            -v ${PWD}/cache:/bughunter \
            -v ${PWD}/debug:/debug \
            -v ${PWD}/results:/results \
            -it \
            christimperley/phd:repair-model
