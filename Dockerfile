FROM christimperley/bughunter:latest
MAINTAINER Chris Timperley "christimperley@gmail.com"

ADD repositories.txt /repositories.txt
ADD experiment.py /entrypoint.py
ENTRYPOINT /entrypoint.py
