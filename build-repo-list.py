#!/usr/bin/python3
#
# Adapted from Rijnard Van Tonder's script: https://github.com/rvantonder/github-scripts
import json
import urllib.request
import sys

LANG = "C"
REQ_URL = 'https://api.github.com/search/repositories?sort=stars&order=desc&q=language:%s&page=%d&per_page=1000'

def url_with_access(url):
  with open("token.txt", "r") as f:
    token = f.readline().strip()
  return url.replace('?','?access_token=%s&' % token)

def read(url):
  req = urllib.request.Request(url)
  response = urllib.request.urlopen(req)
  return response.read().decode('utf-8')

def flatten(l):
  return [item for sublist in l for item in sublist]

def read_page(page):
  url = url_with_access(REQ_URL % (LANG, page))
  res = json.loads(read(url))
  repos = []
  for repo in res["items"]:
    repos.append(repo["clone_url"])
  return repos

if __name__ == '__main__':
  if len(sys.argv) == 1:
    print("Usage: ./build-repo-list.py <number (max = 1000)>")
  else:
    number = int(sys.argv[1])
    pages = number // 100
    repos = []
    for page in range(1, pages + 1):
        repos += read_page(page)

    # write results to file, overwriting any existing results
    with open("repositories.txt", "w") as f:
        f.seek(0)
        for i in range(number - 1):
            repos[i] = "%s\n" % repos[i]
        f.writelines(repos)
        f.truncate()
