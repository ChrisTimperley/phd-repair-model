#!/usr/bin/env python3
from collections import Counter
from multiprocessing.pool import ThreadPool
from pprint import pprint
import random
import traceback
import json
from bughunter.bughunter import BugHunter
from bughunter.repository import Repository
from bughunter.action import RepairActions
from bughunter.pool import ConcreteDonorPoolSet, AbstractDonorPoolSet

bh = BugHunter()

# Samples a given number of fixes from a list of repositories at random
def sample_fixes(repo_urls, size):
    bugs = []
    for url in repo_urls:
        bugs += Repository(bh, url).fixes()
    return random.sample(bugs, size)

#
class ErrorLog(object):
    def __init__(self):
        self.log = []

    def report(self, repo, fix, exc, trace):
        print("ERROR: %s (%s)" % (fix.identifier(), repo.address()))
        self.log.append({
            'type': 'fix',
            'repo': {
                'id': repo.id(),
                'address': repo.address(),
            },
            'fix': fix.identifier(),
            'error': str(exc),
            'trace': trace
        })

    def save(self):
        with open("/debug/errors.json", "w") as f:
            json.dump(self.log, f, indent=2, sort_keys=True)

ERRORS = ErrorLog()

###############################################################################
# FIX MINING
###############################################################################

# Aggregate each bug fix into a count of the number of bugs within each project
def find_bug_count(bh, repo_urls):
    bug_count_by_project = {repo_url: len(bh.repository(repo_url).fixes()) \
                            for repo_url in repo_urls}
    with open("/results/bug_count.json", "w") as f:
        jsn = {'project': bug_count_by_project,
               'total': len(bug_count_by_project.values())}
        json.dump(jsn, f, indent=2, sort_keys=True)
    print("Wrote bug count to file")

# Generates and returns a new repair action counter
def repair_action_counter():
    return Counter({action_type.__name__: 0 for action_type in RepairActions.ACTIONS})

# MAIN
try:
    # Fetch the list of repository URLs
    with open("/repositories.txt", "r") as f:
        repo_urls = [line.rstrip('\n') for line in f]

    # seed the RNG
    random.seed(0)

    # Generate a random sample of bug fixes
    sample = sample_fixes(repo_urls, 2000)

    # Track results
    bugs = 0
    repair_action_instances = repair_action_counter()
    repair_actions_used = repair_action_counter()

    for (i, fix) in enumerate(sample):
        repo = fix.repository()
        try:
            print("Bug %d: %s (%s)" % (i, fix.identifier(), repo.address()))
            instances = repair_action_counter()
            for diff in fix.diffs():
                abstract_pool = diff.abstract_pool()
                concrete_pool = diff.concrete_pool()
                instances.update(diff.actions().stats())
           
            # update the total instances counter
            bugs += 1
            repair_action_instances += instances

            # find which repair actions were used
            for (action_name, count) in instances.items():
                if count > 0:
                    repair_actions_used[action_name] += 1

        except (KeyboardInterrupt, SystemExit):
            raise
        except Exception as e:
            ERRORS.report(repo, fix, e, traceback.format_exc())

    # calculate percentage of bugs using a repair action
    repair_actions_used_percentage = \
        {n: c / float(bugs) for (n, c) in repair_actions_used.items()}

    # print the results to screen
    print("BUGS PROCESSED: %d\n" % bugs)

    print("REPAIR ACTION INSTANCES:")
    pprint(repair_action_instances)

    print("\nREPAIR ACTIONS USAGE:")
    pprint(repair_actions_used)

    # save results to file
    with open("/results/repair_action_stats.json", "w") as f:
        jsn = {
            'bugs': bugs,
            'instances': repair_action_instances,
            'usage': repair_actions_used,
            'usage_fraction': repair_actions_used_percentage
        }
        json.dump(jsn, f, indent=2, sort_keys=True)

finally:
    ERRORS.save()
