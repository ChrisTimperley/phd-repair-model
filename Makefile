all: docker-build repositories.txt

NUM_REPOS=200

repositories.txt:
	python3 build-repo-list.py ${NUM_REPOS}

docker-build: repositories.txt
	docker build -t christimperley/phd:repair-model .

PHONY: docker-build
